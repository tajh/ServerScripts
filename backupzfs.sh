#!/usr/bin/env bash

# ZFS backup script v1.01
# MAINTAINER tajh@leit.so

TARGETTEST="*@*";
if [[ $1 != $TARGETTEST ]]; then
  echo "Usage:"
  echo " $0 <USER@HOST> [zfs filesystem] [target path prefix]"
  echo
  echo " e.g. $0 backup@bkupserver.blaze.ca myfilesystem ${HOSTNAME}-backup"
  echo
  exit 1
fi

TARGET=$1

echo "Target system: $TARGET"

if [ -z "$2" ]; then
  JAILLIST=$(zfs list -r -H -o name zroot | egrep "ezjail" | egrep $(jls | awk 'NR > 1{print $3}' | paste -s -d'|' -) | paste -d ' ' -s -)
else
  JAILLIST=$2
fi
echo "Filesystem(s) to copy: "
echo $JAILLIST | tr ' ' '\n'
echo

if [ ! -z $3 ]; then
  TARGETPATH=$3
  echo "Target path specified: $TARGETPATH"
fi

for JAILTOCOPY in $JAILLIST
do
  # work on this jail
  echo "Starting work on $JAILTOCOPY at `date`"
  FIRSTSNAP=`zfs list -t snapshot -o name | egrep -m1 "^${JAILTOCOPY}@"`
  LASTSNAP=`zfs list -t snapshot -o name | egrep "^${JAILTOCOPY}@" | awk 'END{print}'`
  echo " First local snapshot: $FIRSTSNAP"
  echo " Last local snapshot: $LASTSNAP"
  echo " Testing/creating remote file system: ${TARGET} : ${TARGETPATH}${JAILTOCOPY}"
  EXISTENCETEST="*dataset already exists"

  RESULT=$(ssh -4 -q -i /root/.ssh/id_ed25519 $TARGET sudo zfs create -p -o compress=lz4 ${TARGETPATH}${JAILTOCOPY} 2>&1)

  if [ $? = 0 ] || { [ $? = 1 ] && [[ $RESULT == $EXISTENCETEST ]] ; }; then
    # get the latest snapshot from the target
    TARGETLASTSNAP=`ssh -4 -q -i /root/.ssh/id_ed25519 $TARGET zfs list -t snapshot -o name | grep ${TARGETPATH}${JAILTOCOPY} | awk 'END{print}'`
    SNAPTARGETLASTDATE=$( echo "${TARGETLASTSNAP}" | cut -d'@' -f2 )
    SNAPLOCALLASTDATE=$( echo "${LASTSNAP}" | cut -d'@' -f2 )
    LOCALSNAPWITHTARGETDATE=${JAILTOCOPY}@${SNAPTARGETLASTDATE}
    echo " Last snapshot on target: ${TARGETLASTSNAP}"
    if [ -z $TARGETLASTSNAP ]; then
      echo -n "  There are no snapshots on the target.  Sending first local snapshot to target: "; date;
      COMMAND="zfs send $FIRSTSNAP | lzma --best | ssh -4 -q -i /root/.ssh/id_ed25519 $TARGET \"lzcat | sudo zfs recv -F ${TARGETPATH}${JAILTOCOPY}\""
      #      echo $COMMAND
      eval ${COMMAND}
      echo -n "  Completed first snapshot "; date
      echo
      echo "  Sending the rest of the snapshots:"
      COMMAND="zfs send -I $FIRSTSNAP $LASTSNAP | lzma --best | ssh -4 -q -i /root/.ssh/id_ed25519 $TARGET \"lzcat | sudo zfs recv -F ${TARGETPATH}${JAILTOCOPY}\""
      #      echo $COMMAND
      eval ${COMMAND}
      echo -n "  Completed remaining snapshots "; date
    else
      echo "  Last snapshot on target: $TARGETLASTSNAP"
      if [[ $SNAPTARGETLASTDATE == $SNAPLOCALLASTDATE ]]; then
        echo "  Matches latest on local: $LASTSNAP"
      else
        echo "  Last snapshot on local: $LASTSNAP"
        COMMAND="zfs send -I ${LOCALSNAPWITHTARGETDATE} $LASTSNAP | lzma --best | ssh -4 -q -i /root/.ssh/id_ed25519 $TARGET \"lzcat | sudo zfs recv -F ${TARGETPATH}${JAILTOCOPY}\""
        #        echo $COMMAND
        eval ${COMMAND}
        echo -n "  Completed remaining snapshots "; date
        echo
      fi
    fi
    NEWTARGETLASTSNAP=`ssh $TARGET zfs list -t snapshot -o name | grep ${TARGETPATH}${JAILTOCOPY} | awk 'END{print}'`
    echo " The latest snapshot of $JAILTOCOPY on the target is now: $NEWTARGETLASTSNAP"
  else
    echo "** There was an error creating the file system on the target"
  fi

  echo -n "Finished work on file system $JAILTOCOPY "; date;
  echo
done
